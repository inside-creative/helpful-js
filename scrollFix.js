/**
 * scrollFix
 * =========
 *
 * USAGE
 * =====
 *
 * $('.yourEle').scrollFix();
 *
 * OPTIONS
 * =======
 *
 * offset: Number of pixels from the top of the window before attaching the 'fix' class- default: 250
 * class: Class to add or remove- default: fixed
*/


;(function($) {
    'use strict';

    $.fn.scrollFix = function( options ) {

        // Establish our default settings
        var settings = $.extend({
            offset         : 250,
            class        : 'fixed',
        }, options);

        var elem = this;

        $(window).scroll(function(){
            if ( $(this).scrollTop() > settings.offset ) {
                 elem.addClass(settings.class);
            } else {
                 elem.removeClass(settings.class);
            }
        });
        return this;
    }
}(jQuery));

/**
 * Data BG
 * =======
 *
 * Easily add background images to your website using simple data attirbutes.
 *
 * USAGE
 * =====
 *
 * <div class="yourClass" data-src="/path/to/img.jpg" data-attachment="fixed" data-height="500px" data-offset="center">
 *
 * $(.'yourClass').dataBg();
 *
 * OPTIONS
 * =======
 * You can use the following data attributes
 *
 * data-src: enter the path to your image here
 * data-height: Enter a height for the div i.e. 200px / 50% / 20em
 * data-attachment: CSS attachment i.e. fixed / scroll - default:scroll
 * data-offset: Takes default css offsets i.e. top / center / bottom or 200px etc
 *
*/


;(function ($) {
    'use strict';
    $.fn.dataBg = function () {

        var offset = 'center';
        var attachment = 'scroll';
        var height = 'auto';

  // Run the loop on the bgClass
        return this.each( function () {
            var bgImg = $(this).data('src');
            var bgAtt = $(this).data('attachment') === undefined ? attachment : $(this).data('attachment');
            var bgHeight = $(this).data('height') === undefined ? height : $(this).data('height');
            var bgPos = $(this).data('offset') === undefined ? offset : $(this).data('offset');
            $(this).css({
                "background-image": "url(" + bgImg + ")",
                "background-position": bgPos,
                "background-attachment": bgAtt,
                "min-height": bgHeight
            });
        });
    };
}(jQuery));

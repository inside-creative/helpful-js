/**
 * scrollFix
 * =========
 *
 * USAGE
 * =====
 *
 * $('.yourEle').scrollTo();
 *
 * OPTIONS
 * =======
 *
 * Speed: Speed at which to scroll
*/

;(function($) {
        'use strict';

        $.fn.scrollTo = function( options ) {

            // Establish our default settings
            var settings = $.extend({
                speed         : 1000,
            }, options);

        return this.click(function(e){
            e.preventDefault();
            var _target = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(_target).offset().top
            }, settings.speed);
        });
    }

}(jQuery));
